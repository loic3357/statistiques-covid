module.exports = {
    outputDir: '../cordova/www',
    publicPath: './',

    pluginOptions: {
      quasar: {
        importStrategy: 'kebab',
        rtlSupport: true
      }
    },

    transpileDependencies: [
      'quasar'
    ]
}
