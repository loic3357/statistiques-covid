import VueRouter from "vue-router";

import Home from "../pages/Home";
import DetailRegion from "@/pages/DetailRegion";
import DetailDepartement from "@/pages/DetailDepartement";
import Attestation from "@/pages/Attestation";
import Test from "@/pages/Test";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/region=:nameRegion - :codeRegion',
        name: 'Region',
        component: DetailRegion
    },
    {
        path: '/departement=:nameDepartement',
        name: 'Departement',
        component: DetailDepartement
    },
    {
        path: '/attestation',
        name: 'Attestation',
        component: Attestation
    },
    {
        path: '/test',
        name: 'test',
        component: Test
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
