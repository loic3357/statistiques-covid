import Vue from 'vue';
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue'

import router from './config/router';
import { store } from "./store/store";
import './config/init';
import './quasar'

Vue.config.productionTip = false;
Vue.use(BootstrapVue)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
